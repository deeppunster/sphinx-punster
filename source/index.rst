.. dojo documentation master file, created by
   sphinx-quickstart on Thu Jun 11 20:19:39 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dojo's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Foomatic <README.rst>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
